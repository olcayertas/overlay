//
//  OverlayBlendView.swift
//  Overlay
//
//  Created by Olcay Ertaş on 17.05.2020.
//  Copyright © 2020 OverlayApp. All rights reserved.
//

import UIKit

class ScrollableOverLayBlendView: UIScrollView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    var overlayBlendView = OverlayBlendView()

    var topConstraint: NSLayoutConstraint!
    var heightConstraint: NSLayoutConstraint!

    private func setup() {
        overlayBlendView.translatesAutoresizingMaskIntoConstraints = false
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(overlayBlendView)
        overlayBlendView.leadingAnchor.constraint(equalTo: contentLayoutGuide.leadingAnchor).isActive = true
        overlayBlendView.trailingAnchor.constraint(equalTo: contentLayoutGuide.trailingAnchor).isActive = true
        topConstraint = overlayBlendView.topAnchor.constraint(equalTo: contentLayoutGuide.topAnchor)
        topConstraint.isActive = true
        heightConstraint = overlayBlendView.heightAnchor.constraint(equalToConstant: bounds.height)
        heightConstraint.isActive = true
        overlayBlendView.widthAnchor.constraint(equalToConstant: bounds.width).isActive = true
        //Disable scroll when panning overlay
        if let recognizer = overlayBlendView.panRecognizer {
            panGestureRecognizer.require(toFail: recognizer)
        }
    }

    public func setImage(_ image: UIImage) {
        if image.size.height > image.size.width && image.size.height > bounds.height {
            let contentHeight = floor((bounds.width * image.size.height) / image.size.width)
            topConstraint.isActive = false
            topConstraint = overlayBlendView.topAnchor.constraint(equalTo: contentLayoutGuide.topAnchor, constant: 0)
            topConstraint.isActive = true
            heightConstraint.isActive = false
            heightConstraint = overlayBlendView.heightAnchor.constraint(equalToConstant: contentHeight)
            heightConstraint.isActive = true
            contentSize = CGSize(width: bounds.width, height: contentHeight)
            isScrollEnabled = true
        } else if image.size.height > image.size.width {
            topConstraint.isActive = false
            topConstraint = overlayBlendView.topAnchor.constraint(equalTo: contentLayoutGuide.topAnchor, constant: 0)
            topConstraint.isActive = true
            heightConstraint.isActive = false
            heightConstraint = overlayBlendView.heightAnchor.constraint(equalToConstant: bounds.height)
            heightConstraint.isActive = true
            contentSize = bounds.size
            isScrollEnabled = false
        } else {
            let contentHeight = floor((bounds.width * image.size.height) / image.size.width)
            let top = (bounds.height - contentHeight) / 2
            topConstraint.isActive = false
            topConstraint = overlayBlendView.topAnchor.constraint(equalTo: contentLayoutGuide.topAnchor, constant: top)
            topConstraint.isActive = true
            heightConstraint.isActive = false
            heightConstraint = overlayBlendView.heightAnchor.constraint(equalToConstant: contentHeight)
            heightConstraint.isActive = true
            contentSize = bounds.size
            isScrollEnabled = false
        }
        overlayBlendView.setImage(image: image)
    }

    public func setOverlayImage(image: UIImage) {
        overlayBlendView.setOverlayImage(image: image)
    }

    func saveImage(onImageSaved: (() -> Void)?) {
        overlayBlendView.saveImage(onImageSaved: onImageSaved)
    }
}


class OverlayBlendView: UIView {

    private var imageView = UIImageView()
    private var overlayImageView = UIImageView()

    var panRecognizer: UIPanGestureRecognizer?
    var pinchRecognizer: UIPinchGestureRecognizer?
    var rotateRecognizer: UIRotationGestureRecognizer?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        overlayImageView.center = lastCenter == .zero ? self.center : lastCenter
        overlayImageView.transform = lastTransform
    }

    private func setup() {

        clipsToBounds = true

        imageView.accessibilityIdentifier = "Original Image View"
        addImageView(imageView: imageView)
        overlayImageView.accessibilityIdentifier = "Overlay Image View"
        addImageView(imageView: overlayImageView)

        self.panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(recognizer:)))
        self.pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.handlePinch(recognizer:)))
        self.rotateRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(self.handleRotate(recognizer:)))
        //delegate gesture with UIGestureRecognizerDelegate
        pinchRecognizer?.delegate = self
        rotateRecognizer?.delegate = self
        panRecognizer?.delegate = self
        // than add gesture to imgView
        overlayImageView.isUserInteractionEnabled = true
        overlayImageView.isMultipleTouchEnabled = true
        self.overlayImageView.addGestureRecognizer(panRecognizer!)
        self.overlayImageView.addGestureRecognizer(pinchRecognizer!)
        self.overlayImageView.addGestureRecognizer(rotateRecognizer!)

        lastCenter = overlayImageView.center
    }

    private var lastCenter: CGPoint = .zero

    // handle UIPanGestureRecognizer
    @objc func handlePan(recognizer: UIPanGestureRecognizer) {
        let g_view = recognizer.view
        if recognizer.state == .began || recognizer.state == .changed {
            let translation = recognizer.translation(in: g_view?.superview)
            lastCenter = CGPoint(x: (g_view?.center.x)! + translation.x, y: (g_view?.center.y)! + translation.y)
            g_view?.center = lastCenter
            recognizer.setTranslation(CGPoint.zero, in: g_view?.superview)
        }
    }

    var lastTransform: CGAffineTransform = .identity

    // handle UIPinchGestureRecognizer
    @objc func handlePinch(recognizer: UIPinchGestureRecognizer) {
        if recognizer.state == .began || recognizer.state == .changed {
            lastTransform = (recognizer.view?.transform.scaledBy(x: recognizer.scale, y: recognizer.scale))!
            recognizer.view?.transform = lastTransform
            recognizer.scale = 1.0
        }
    }

    var lastRotation: CGFloat = 0.0

    // handle UIRotationGestureRecognizer
    @objc func handleRotate(recognizer: UIRotationGestureRecognizer) {
        if recognizer.state == .began || recognizer.state == .changed {
            lastRotation = recognizer.rotation
            lastTransform = (recognizer.view?.transform.rotated(by: recognizer.rotation))!
            recognizer.view?.transform = lastTransform
            recognizer.rotation = 0.0
        }
    }

    private func addImageView(imageView: UIImageView) {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
        let safeArea = self.safeAreaLayoutGuide
        imageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: bounds.height).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: bounds.width).isActive = true
    }

    public func setImage(image: UIImage) {
        imageView.image = image
    }

    public func setOverlayImage(image: UIImage) {
        overlayImageView.image = image
        overlayImageView.transform = .identity
        overlayImageView.center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
    }

    var onImageSaved: (() -> Void)?

    func saveImage(onImageSaved: (() -> Void)?) {
        self.onImageSaved = onImageSaved
        let selector = #selector(self.onImageSaved(_:error:contextInfo:))
        takeSnapshot()?.saveToPhotoLibrary(self, format: .jpeg(quality: 0.7), selector)
    }

    func save() -> UIImage? {

        guard let bottomImage = imageView.image else {
            return nil
        }

        guard let overlayImage = overlayImageView.image else {
            return nil
        }

        let newSize = overlayImageView.bounds.size

        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)

        let boundsScale = newSize.width / newSize.height
        let imageScale = overlayImage.size.width / overlayImage.size.height

        var canvasSize = overlayImage.size

        if boundsScale > imageScale {
            canvasSize.width = canvasSize.height * boundsScale
        } else {
            canvasSize.height = canvasSize.width / boundsScale
        }

        let xScale = canvasSize.width / imageView.bounds.width
        let yScale = canvasSize.height / imageView.bounds.height

        let center = imageView.center.applying(CGAffineTransform.identity.scaledBy(x: xScale, y: yScale))

        let xCenter = center.x
        let yCenter = center.y

        let context = UIGraphicsGetCurrentContext()!

        //Apply transformation
        context.translateBy(x: xCenter, y: yCenter)
        context.concatenate(overlayImageView.transform)
        context.translateBy(x: -xCenter, y: -yCenter)

        var drawingRect: CGRect = .zero
        drawingRect.size = canvasSize

        //Translation
        drawingRect.origin.x = (xCenter - overlayImage.size.width * 0.5)
        drawingRect.origin.y = (yCenter - overlayImage.size.height * 0.5)

        //Aspect fit calculation
        if boundsScale > imageScale {
            drawingRect.size.width = drawingRect.size.height * imageScale
        } else {
            drawingRect.size.height = drawingRect.size.width / imageScale
        }

        bottomImage.draw(in: CGRect(origin: .zero, size: newSize))
        overlayImage.draw(in: drawingRect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)

        newImage?.draw(in: CGRect(origin: .zero, size: newSize))
        bottomImage.draw(in: CGRect(origin: .zero, size: newSize))

        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return finalImage
    }

    func save(_ topImage: UIImage) -> UIImage? {
        guard let bottomImage = imageView.image else {
            return nil
        }
        let newSize = CGSize(width: bottomImage.size.width, height: bottomImage.size.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        bottomImage.draw(in: CGRect(origin: .zero, size: newSize))
        topImage.draw(in: CGRect(origin: .zero, size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

    @objc private func onImageSaved(_ image: UIImage, error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(
                    title: "Save error",
                    message: error.localizedDescription,
                    preferredStyle: .alert
            )
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            ac.present()
        } else {
            let ac = UIAlertController(
                    title: "Saved!",
                    message: "Your altered image has been saved to your photos.",
                    preferredStyle: .alert
            )
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            ac.present()
        }
        onImageSaved?()
    }
}

extension OverlayBlendView: UIGestureRecognizerDelegate {

    // mark sure you override this function to make gestures work together
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
}
