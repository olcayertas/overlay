//
// Created by Olcay Ertaş on 16.05.2020.
// Copyright (c) 2020 OverlayApp. All rights reserved.
//

import UIKit

extension Encodable {

    func prettyPrint() {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        guard let data = try? encoder.encode(self) else {
            return
        }
        guard let jsonString = String(data: data, encoding: .utf8) else {
            return
        }
        print(jsonString)
    }
}

extension URLRequest {

    public var publisher: URLSession.DataTaskPublisher {
        URLSession.shared.dataTaskPublisher(for: self)
    }
}

extension UserDefaults {

    func clear() {
        guard let domainName = Bundle.main.bundleIdentifier else {
            return
        }
        removePersistentDomain(forName: domainName)
        synchronize()
    }
}

extension UIAlertController {

    func present() {
        let windows = UIApplication.shared.windows
        let firstKeyWindow = windows.filter({ $0.isKeyWindow }).first
        guard let controller = firstKeyWindow?.rootViewController else {
            return
        }
        controller.present(self, animated: true)
    }
}

extension UIView {

    func takeSnapshot() -> UIImage? {
        // Remove bright line at the bottom by reducing context height
        // TODO: Find the reason of the bright line
        UIGraphicsBeginImageContext(CGSize(width: self.frame.size.width, height: self.frame.size.height - 5))
        let rect = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        drawHierarchy(in: rect, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIImage {

    public enum ImageFormat {
        case png
        case jpeg(quality: CGFloat)
    }

    func saveToPhotoLibrary(_ completionTarget: Any?, format: ImageFormat = .png, _ completionSelector: Selector?) {
        DispatchQueue.global(qos: .userInitiated).async {
            var _data: Data? = nil
            switch format {
            case .png:
                _data = self.pngData()
            case .jpeg(quality: let quality):
                _data = self.jpegData(compressionQuality: quality)
            }
            guard let data = _data else {
                return
            }
            guard let compressedImage = UIImage(data: data) else {
                return
            }
            UIImageWriteToSavedPhotosAlbum(compressedImage, completionTarget, completionSelector, nil)
        }
    }


    private func draw(onImage: UIImage, position: CGPoint) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: self.size)
        return renderer.image { context in
            onImage.draw(in: CGRect(origin: CGPoint.zero, size: onImage.size))
            draw(in: CGRect(origin: position, size: self.size))
        }
    }
}