//
//  Overlay.swift
//  Overlay
//
//  Created by Olcay Ertaş on 16.05.2020.
//  Copyright © 2020 OverlayApp. All rights reserved.
//

import Foundation

public struct Overlay: Codable {
    var overlayId: UInt?
    var overlayName: String?
    var overlayPreviewIconUrl: String?
    var overlayUrl: String?
}
