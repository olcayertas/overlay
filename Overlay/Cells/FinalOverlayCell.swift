//
//  OverlayCollectionViewCell.swift
//  Overlay
//
//  Created by Olcay Ertaş on 16.05.2020.
//  Copyright © 2020 OverlayApp. All rights reserved.
//

import UIKit
import Combine

public protocol OverlayCellViewModelProtocol: NSObject {
    var overlayPreviewImageData: Data? { get }
    var overlayImageData: Data? { get }
    func getPreview(imageName: String) -> BoolPublisher
    func getImage(imageName: String) -> BoolPublisher
}

class OverlayCollectionViewCell<ViewModelType: OverlayCellViewModelProtocol>: UICollectionViewCell {

    internal var _overlayNameLabel: UILabel!
    internal var _previewImageView: UIImageView!

    private var viewModel = ViewModelType()
    private var cancelBag = Set<AnyCancellable>()

    public var overlay: Overlay? {
        didSet {
            _overlayNameLabel.text = overlay?.overlayName
            guard let url = overlay?.overlayPreviewIconUrl else {
                return
            }
            if let image = UIImageCache.sharedInstance.uiImage(for: url) {
                _previewImageView.image = image
            }
        }
    }

    deinit {
        cancelBag.forEach { cancellable in
            cancellable.cancel()
        }
    }

    public func highlightImage() {
        _previewImageView.layer.cornerRadius = 10
        _previewImageView.layer.borderWidth = 3
        _previewImageView.layer.borderColor = UIColor.blue.cgColor
    }

    public func unHighlightImage() {
        _previewImageView.layer.cornerRadius = 10
        _previewImageView.layer.borderWidth = 0
        _previewImageView.layer.borderColor = UIColor.blue.cgColor
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        _previewImageView.image = nil
        _previewImageView.layer.borderWidth = 0
        _overlayNameLabel.text = nil
    }
}

final class FinalOverlayCell: OverlayCollectionViewCell<OverlayCellViewModel<OverlayCellApiClient>> {

    @IBOutlet weak var overlayNameLabel: UILabel! {
        didSet {
            _overlayNameLabel = overlayNameLabel
        }
    }

    @IBOutlet weak var previewImageView: UIImageView! {
        didSet {
            _previewImageView = previewImageView
            _previewImageView.layer.cornerRadius = 10
        }
    }
}
