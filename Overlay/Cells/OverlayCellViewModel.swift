//
// Created by Olcay Ertaş on 16.05.2020.
// Copyright (c) 2020 OverlayApp. All rights reserved.
//

import Foundation
import Combine

public protocol OverlayCellApiClientProtocol: NSObject {
    func getImage(imageName: String) -> AnyPublisher<Data, Error>
}

public class OverlayCellViewModel<ApiClientType: OverlayCellApiClientProtocol>: NSObject, OverlayCellViewModelProtocol {

    public var overlayPreviewImageData: Data?
    public var overlayImageData: Data?
    private let apiClient = ApiClientType()

    private var cancelBag = Set<AnyCancellable>()

    deinit {
        cancelBag.forEach { cancellable in
            cancellable.cancel()
        }
    }

    public func getPreview(imageName: String) -> BoolPublisher {
        Future<Bool, Error> { [weak self] promise in
            guard let self = self else {
                return
            }
            self.apiClient.getImage(imageName: imageName).sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    promise(.success(true))
                case .failure(let error):
                    promise(.failure(error))
                }
            }, receiveValue: { data in
                self.overlayPreviewImageData = data
            }).store(in: &self.cancelBag)
        }.receive(on: RunLoop.main).eraseToAnyPublisher()
    }

    public func getImage(imageName: String) -> BoolPublisher {
        Future<Bool, Error> { [weak self] promise in
            guard let self = self else {
                return
            }
            self.apiClient.getImage(imageName: imageName).sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    promise(.success(true))
                case .failure(let error):
                    promise(.failure(error))
                }
            }, receiveValue: { data in
                self.overlayImageData = data
            }).store(in: &self.cancelBag)
        }.receive(on: RunLoop.main).eraseToAnyPublisher()
    }
}