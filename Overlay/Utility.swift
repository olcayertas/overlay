//
// Created by Olcay Ertaş on 16.05.2020.
// Copyright (c) 2020 OverlayApp. All rights reserved.
//

import UIKit

public class UIImageCache {

    private var imageSet = [String: UIImage]()

    public static let sharedInstance = UIImageCache()

    func loadImage(for url: String) {
        if let data = UserDefaults.standard.data(forKey: url) {
            imageSet[url] = UIImage(data: data)
        } else if url.starts(with: "nosign") {
            imageSet[url] = UIImage(systemName: url)
        } else if url.starts(with: "http") == false {
            imageSet[url] = UIImage(named: url)
        } else {
            print("UIImageCache: No cached data for \(url)!")
        }
    }

    func uiImage(for url: String) -> UIImage? {
        if let image = imageSet[url] {
            return image
        } else {
            print("UIImageCache: No cached image for \(url)!")
            return nil
        }
    }
}