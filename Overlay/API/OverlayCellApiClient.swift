//
// Created by Olcay Ertaş on 16.05.2020.
// Copyright (c) 2020 OverlayApp. All rights reserved.
//

import Foundation
import Combine

class OverlayCellApiClient: BaseApiClient, OverlayCellApiClientProtocol {

    func getImage(imageName: String) -> AnyPublisher<Data, Error> {
        fetch(endpoint: imageName)
    }
}
