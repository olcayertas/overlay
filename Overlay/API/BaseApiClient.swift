//
//  BaseApiClient.swift
//  Overlay
//
//  Created by Olcay Ertaş on 16.05.2020.
//  Copyright © 2020 OverlayApp. All rights reserved.
//

import Foundation
import Combine

enum ApiClientError: Error, LocalizedError {

    case failedToGetSelf
    case failedToGetBaseUrl
    case failedToCreateUrl(String)
    case failedToDecodeResponse
    case networkError(URLError)
    case apiError(String)
    case unknown

    var errorDescription: String? {
        switch self {
        case .unknown:
            return "Unknown error"
        case .apiError(let error):
            return error
        case .failedToGetBaseUrl:
            return "Did you set base URL?"
        case .failedToGetSelf:
            return "Failed to get api client self!"
        case .failedToCreateUrl(let endpoint):
            return "Invalid URL: \(endpoint)"
        case .failedToDecodeResponse:
            return "Failed to decode response!"
        case .networkError(let error):
            return error.localizedDescription
        }
    }
}

public class BaseApiClient: NSObject {

    internal let baseUrl = URL(string: "https://add.your.api.url.here")!
    internal var cancelBag = Set<AnyCancellable>()

    deinit {
        cancelBag.forEach { cancellable in
            cancellable.cancel()
        }
    }

    override init() {
        URLSession.shared.configuration.shouldUseExtendedBackgroundIdleMode = true
    }

    public func invalidateCache() {
        UserDefaults.standard.clear()
    }

    private func handleResponse(endpoint: String, response: URLResponse) throws {
        guard let httpResponse = response as? HTTPURLResponse else {
            throw ApiClientError.unknown
        }
        if (httpResponse.statusCode == 401) {
            throw ApiClientError.apiError("Unauthorized");
        } else if (httpResponse.statusCode == 403) {
            throw ApiClientError.apiError("Resource forbidden");
        } else if (httpResponse.statusCode == 404) {
            throw ApiClientError.apiError("Resource not found");
        } else if (405..<500 ~= httpResponse.statusCode) {
            throw ApiClientError.apiError("client error");
        } else if (500..<600 ~= httpResponse.statusCode) {
            throw ApiClientError.apiError("server error");
        }
    }

    private func handleError(error: Error) -> Error {
        if let error = error as? ApiClientError {
            return error
        }
        if let urlError = error as? URLError {
            return ApiClientError.networkError(urlError)
        }
        return ApiClientError.unknown
    }

    private func basePublisher(url: URL) -> Publishers.TryMap<URLSession.DataTaskPublisher, Data> {
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        return request.publisher.tryMap { data, response in
            try self.handleResponse(endpoint: url.path, response: response)
            UserDefaults.standard.setValue(data, forKey: url.absoluteString)
            UserDefaults.standard.synchronize()
            return data
        }
    }

    internal func fetch<T: Codable>(endpoint: String, type: T.Type) -> AnyPublisher<T, Error> {

        Future<T, Error> { [weak self] promise in
            guard let self = self else {
                promise(.failure(ApiClientError.failedToGetSelf))
                return
            }
            if let data = UserDefaults.standard.data(forKey: endpoint) {
                if let model = try? JSONDecoder().decode(type, from: data) {
                    promise(.success(model))
                    print("Returned data from cache")
                    return
                }
            }
            let url = self.baseUrl.appendingPathComponent(endpoint)
            self.basePublisher(url: url)
                    .decode(type: T.self, decoder: JSONDecoder())
                    .mapError {
                        self.handleError(error: $0)
                    }
                    .eraseToAnyPublisher()
                    .sink(receiveCompletion: { completion in
                        switch completion {
                        case .failure(let error):
                            promise(.failure(error))
                        case .finished:
                            break
                        }
                    }, receiveValue: { overlays in
                        promise(.success(overlays))
                    })
                    .store(in: &self.cancelBag)
        }.eraseToAnyPublisher()
    }

    private func _fetch(url: URL, _completion: ((Error?, Data?) -> Void)? = nil) {
        self.basePublisher(url: url)
                .mapError {
                    self.handleError(error: $0)
                }
                .eraseToAnyPublisher()
                .sink(receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        print("BaseApiClient : _fetch : failed : \(error.localizedDescription)")
                        _completion?(error, nil)
                    case .finished:
                        print("BaseApiClient : _fetch : finished")
                    }
                }, receiveValue: { data in
                    print("BaseApiClient : _fetch : received data")
                    _completion?(nil, data)
                })
                .store(in: &self.cancelBag)
    }

    internal func fetch(endpoint: String) -> AnyPublisher<Data, Error> {
        Future<Data, Error> { [weak self] promise in
            guard let self = self else {
                promise(.failure(ApiClientError.failedToGetSelf))
                return
            }
            guard let url = URL(string: endpoint) else {
                promise(.failure(ApiClientError.failedToCreateUrl(endpoint)))
                return
            }
            if let data = UserDefaults.standard.data(forKey: endpoint) {
                print("BaseApiClient : Returning data from cache")
                promise(.success(data))
                self._fetch(url: url)
            }
            self._fetch(url: url) { error, data in
                if let error = error {
                    promise(.failure(error))
                }
                if let data = data {
                    promise(.success(data))
                }
            }
        }.eraseToAnyPublisher()
    }
}
