//
// Created by Olcay Ertaş on 16.05.2020.
// Copyright (c) 2020 OverlayApp. All rights reserved.
//

import Foundation
import Combine

public class OverlayApiClient: BaseApiClient, OverlayApiClientProtocol {

    public func getOverlays() -> AnyPublisher<[Overlay], Error> {
        fetch(endpoint: "candidates/overlay.json", type: [Overlay].self)
    }

    public func getImage(url: String) -> AnyPublisher<Data, Error> {
        fetch(endpoint: url)
    }
}