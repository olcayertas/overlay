//
//  OverlayViewController.swift
//  Overlay
//
//  Created by Olcay Ertaş on 16.05.2020.
//  Copyright © 2020 OverlayApp. All rights reserved.
//

import UIKit
import Combine

public typealias BoolPublisher = AnyPublisher<Bool, Error>

public protocol OverlayViewModelProtocol: NSObject {
    var overlays: [Overlay] { get }
    func getOverlays() -> BoolPublisher
    func getAllOverlayImages() -> BoolPublisher
    func loadLocalOverlayImages()
}

class OverlayViewController<ViewModelType: OverlayViewModelProtocol>: UIViewController,
        UICollectionViewDelegate,
        UICollectionViewDataSource,
        UICollectionViewDataSourcePrefetching,
        UIImagePickerControllerDelegate,
        UINavigationControllerDelegate {

    var _overlaysCollectionView: UICollectionView!
    var _activityIndicator: UIActivityIndicatorView!
    var _selectImageButton: UIButton!
    var _scrollView: ScrollableOverLayBlendView!

    private var viewModel = ViewModelType()
    private let cellIdentifier = "FinalOverlayCell"
    private var cancelBag = Set<AnyCancellable>()
    private var selectedCellIndex = -1
    private var didSelectImage = false

    deinit {
        cancelBag.forEach { cancellable in
            cancellable.cancel()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        _selectImageButton.isHidden = true
        showActivityIndicator()
        //getOverlays()
        onOverlayReceived()
    }

    func showActivityIndicator() {
        let activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        activityView.center = self.view.center
        activityView.color = .white
        self.view.addSubview(activityView)
        activityView.startAnimating()
        _activityIndicator = activityView
    }

    func configureCollectionView() {
        let nib = UINib(nibName: cellIdentifier, bundle: .main)
        _overlaysCollectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        _overlaysCollectionView.dataSource = self
        _overlaysCollectionView.delegate = self
        _overlaysCollectionView.prefetchDataSource = self
    }

    func getOverlays() {
        viewModel.getOverlays().sink(receiveCompletion: { [weak self] _ in
            self?.onOverlayReceived()
        }, receiveValue: { _ in
            print("OverlayViewController: Received overlays!")
        }).store(in: &cancelBag)
    }

    func onOverlayReceived() {
        viewModel.loadLocalOverlayImages()
        onLoaded()
        /* Call this if you have a remote API
        viewModel.getAllOverlayImages().sink(receiveCompletion: { [weak self] _ in
            self?.onLoaded()
        }, receiveValue: { _ in
            print("OverlayViewController: Received overlay images!")
        }).store(in: &cancelBag)
        */
    }

    private func onLoaded() {
        configureCollectionView()
        _selectImageButton.isHidden = false
        _activityIndicator.removeFromSuperview()
        _overlaysCollectionView.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.overlays.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? FinalOverlayCell, indexPath.row < viewModel.overlays.count {
            cell.overlay = viewModel.overlays[indexPath.row]
            if selectedCellIndex == indexPath.row {
                cell.highlightImage()
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? FinalOverlayCell {
            if indexPath.row == selectedCellIndex {
                return
            }
            selectedCellIndex = indexPath.row
            cell.highlightImage()
            if let url = viewModel.overlays[indexPath.row].overlayUrl,
               let image = UIImageCache.sharedInstance.uiImage(for: url),
               didSelectImage {
                if indexPath.row == 0 {
                    _scrollView.setOverlayImage(image: UIImage())
                } else {
                    _scrollView.setOverlayImage(image: image)
                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? FinalOverlayCell {
            cell.unHighlightImage()
        }
    }

    internal func _selectImage() {
        let alertController = UIAlertController(
                title: "Source Type",
                message: "Please select image source type",
                preferredStyle: .actionSheet
        )
        alertController.addAction(UIAlertAction(title: "Photo Library", style: .default) {
            [weak self] _ in
            self?._activityIndicator.isHidden = false
            self?._activityIndicator.startAnimating()
            self?.showPicker(source: .photoLibrary)
        })
        alertController.addAction(UIAlertAction(title: "Saved Photos Album", style: .default) {
            [weak self] _ in
            self?._activityIndicator.isHidden = false
            self?._activityIndicator.startAnimating()
            self?.showPicker(source: .savedPhotosAlbum)
        })
        alertController.addAction(UIAlertAction(title: "Camera", style: .default) {
            [weak self] _ in
            #if targetEnvironment(simulator)
            self?.showCameraNotSupported()
            #else
            self?.showPicker(source: .camera)
            #endif
        })
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alertController.dismiss(animated: true)
        })
        present(alertController, animated: true)
    }

    private func showCameraNotSupported() {
        let controller = UIAlertController(
                title: "Error",
                message: "Simulator doesn't support camera!",
                preferredStyle: .alert
        )
        controller.addAction(UIAlertAction(title: "OK", style: .default) {
            action in
            controller.dismiss(animated: true)
        })
        present(controller, animated: true)
    }

    private func showPicker(source: UIImagePickerController.SourceType) {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = source
        present(controller, animated: true) { [weak self] in
            self?._activityIndicator.stopAnimating()
        }
    }

    typealias MediaInfo = [UIImagePickerController.InfoKey: Any]
    typealias Key = UIImagePickerController.InfoKey

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: MediaInfo) {
        guard let image = info[Key.originalImage] as? UIImage else {
            return
        }
        _scrollView.setImage(image)
        _overlaysCollectionView.isUserInteractionEnabled = true
        _selectImageButton.isHidden = true
        didSelectImage = true
        picker.dismiss(animated: true)
    }

    func _saveImageWithOverlay() {
        _activityIndicator.startAnimating()
        _scrollView.saveImage { [weak self] in
            self?._activityIndicator.stopAnimating()
        }
    }
}

