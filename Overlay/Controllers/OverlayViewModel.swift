//
//  OverlayViewModel.swift
//  Overlay
//
//  Created by Olcay Ertaş on 16.05.2020.
//  Copyright © 2020 OverlayApp. All rights reserved.
//

import Foundation
import Combine

public enum OverlayViewModelError: Error {
    case failedToGetApiClient
    case failedToGetOverlayModels
    case failedToGetSelf
}

public protocol OverlayApiClientProtocol: NSObject {
    func getOverlays() -> AnyPublisher<[Overlay], Error>
    func getImage(url: String) -> AnyPublisher<Data, Error>
}

public class OverlayViewModel<ApiClientType: OverlayApiClientProtocol>: NSObject, OverlayViewModelProtocol {

    public var overlays: [Overlay] = []
    private var apiClient = ApiClientType()
    private var cancelBag = Set<AnyCancellable>()

    deinit {
        cancelBag.forEach { cancellable in
            cancellable.cancel()
        }
    }

    public func getOverlays() -> BoolPublisher {
        print("OverlayViewModel: Getting overlays...")
        return Future<Bool, Error> { [weak self] promise in
            guard let self = self else {
                promise(.failure(OverlayViewModelError.failedToGetSelf))
                return
            }
            self.apiClient.getOverlays().sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    promise(.success(true))
                case .failure(let error):
                    promise(.failure(error))
                }
            }, receiveValue: { overlays in
                print("OverlayViewModel: Received overlays!")
                self.overlays = overlays
            }).store(in: &self.cancelBag)
        }.receive(on: RunLoop.main).eraseToAnyPublisher()
    }

    var combinedPublishers: Publishers.MergeMany<AnyPublisher<Data, Error>> {
        let publishers1: [AnyPublisher<Data, Error>] = overlays.map { overlay in
            print("Creating publisher for \(overlay.overlayName ?? "X")")
            return apiClient.getImage(url: overlay.overlayPreviewIconUrl ?? "")
        }
        let publishers2: [AnyPublisher<Data, Error>] = overlays.map { overlay in
            print("Creating publisher for \(overlay.overlayName ?? "X")")
            return apiClient.getImage(url: overlay.overlayUrl ?? "")
        }
        return Publishers.MergeMany(publishers1 + publishers2)
    }

    /// You can add many items as you need
    public func loadLocalOverlayImages() {
        var overlay = Overlay(
                overlayId: 0,
                overlayName: "Search",
                overlayPreviewIconUrl: "search",
                overlayUrl: "search"
        )
        loadImage(overlay.overlayUrl)
        overlays.insert(overlay, at: 0)

        overlay = Overlay(
                overlayId: 0,
                overlayName: "Address",
                overlayPreviewIconUrl: "address",
                overlayUrl: "address"
        )
        loadImage(overlay.overlayUrl)
        overlays.insert(overlay, at: 0)

        overlay = Overlay(
                overlayId: 0,
                overlayName: "Complaint",
                overlayPreviewIconUrl: "complaint",
                overlayUrl: "complaint"
        )
        loadImage(overlay.overlayUrl)
        overlays.insert(overlay, at: 0)

        overlay = Overlay(
                overlayId: 0,
                overlayName: "Consent",
                overlayPreviewIconUrl: "consent",
                overlayUrl: "consent"
        )
        loadImage(overlay.overlayUrl)
        overlays.insert(overlay, at: 0)

        overlay = Overlay(
                overlayId: 0,
                overlayName: "Detective",
                overlayPreviewIconUrl: "detective",
                overlayUrl: "detective"
        )
        loadImage(overlay.overlayUrl)
        overlays.insert(overlay, at: 0)

        overlay = Overlay(
                overlayId: 0,
                overlayName: "Eraser",
                overlayPreviewIconUrl: "eraser",
                overlayUrl: "eraser"
        )
        loadImage(overlay.overlayUrl)
        overlays.insert(overlay, at: 0)

        overlay = Overlay(
                overlayId: 0,
                overlayName: "Information",
                overlayPreviewIconUrl: "information",
                overlayUrl: "information"
        )
        loadImage(overlay.overlayUrl)
        overlays.insert(overlay, at: 0)

        overlay = Overlay(
                overlayId: 0,
                overlayName: "Right",
                overlayPreviewIconUrl: "right",
                overlayUrl: "right"
        )
        loadImage(overlay.overlayUrl)
        overlays.insert(overlay, at: 0)

        loadNoSignOverlay()
    }

    private func loadNoSignOverlay() {
        let overlay = Overlay(
                overlayId: 2,
                overlayName: "None",
                overlayPreviewIconUrl: "nosign",
                overlayUrl: "nosign"
        )
        overlays.insert(overlay, at: 0)
        loadImage(overlay.overlayUrl)
    }

    public func getAllOverlayImages() -> BoolPublisher {
        print("OverlayViewModel: Getting overlay images...")
        return Future<Bool, Error> { [weak self] promise in
            guard let self = self else {
                promise(.failure(OverlayViewModelError.failedToGetSelf))
                return
            }
            self.combinedPublishers.collect().sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    print("OverlayViewModel: Finished getting overlay images!")
                    break
                case .failure(let error):
                    print("OverlayViewModel: \(error.localizedDescription)")
                    promise(.failure(error))
                }
            }, receiveValue: { results in
                print("OverlayViewModel: Received overlay images!")
                self.overlays.forEach { overlay in
                    self.loadImage(overlay.overlayUrl)
                    self.loadImage(overlay.overlayPreviewIconUrl)
                }
                self.loadNoSignOverlay()
                promise(.success(true))

            }).store(in: &self.cancelBag)
        }.receive(on: RunLoop.main).eraseToAnyPublisher()
    }

    private func loadImage(_ url: String?) {
        guard let url = url else {
            print("OverlayViewModel : WARNING : Failed to get url!")
            return
        }
        UIImageCache.sharedInstance.loadImage(for: url)
    }
}
