//
// Created by Olcay Ertaş on 17.06.2020.
// Copyright (c) 2020 OverlayApp. All rights reserved.
//

import UIKit

final class FinalOverlayViewController: OverlayViewController<OverlayViewModel<OverlayApiClient>> {

    @IBOutlet weak var overlaysCollectionView: UICollectionView! {
        didSet {
            _overlaysCollectionView = overlaysCollectionView
        }
    }

    @IBOutlet weak var selectImageButton: UIButton! {
        didSet {
            _selectImageButton = selectImageButton
        }
    }

    @IBOutlet weak var scrollView: ScrollableOverLayBlendView! {
        didSet {
            _scrollView = scrollView
        }
    }

    @IBAction func saveImageWithOverlay() {
        _saveImageWithOverlay()
    }

    @IBAction func selectImage() {
        _selectImage()
    }
}
