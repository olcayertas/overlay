# Overlay - UIKit & MVVM & Combine
> A simple iOS application to add an overlay image to an image then export it to photo library.

[![Swift Version][swift-image]][swift-url]
[![Build Status][travis-image]][travis-url]
[![License][license-image]][license-url]
[![Platform](https://img.shields.io/badge/Platform-iOS-green)]()
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)]()

This simple app can do followings:

![Screenshot](images/ss.png)

## Features

- [x] Select image from Photo Library, Saved Photos Album or Camera
- [x] Pan, rotate or scale selected overlay
- [x] Save resulting image to Photo Library

## Requirements

- iOS 13.0+
- Xcode 11.5

## Installation

#### Manually
1. Download and open in Xcode.
2. No third party library is required.

## Contribute

I would love you for the contribution, check the ``LICENSE`` file for more info.

## Meta

Olcay Ertaş – [@Linkedin](https://www.linkedin.com/in/olcayertas/) – olcayertas@gmail.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://github.com/olcayertas](https://github.com/olcayertas)

[swift-image]:https://img.shields.io/badge/swift-5.0-orange.svg
[swift-url]: https://swift.org/
[license-image]: https://img.shields.io/badge/License-MIT-blue.svg
[license-url]: LICENSE
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[codebeat-image]: https://codebeat.co/badges/c19b47ea-2f9d-45df-8458-b2d952fe9dad
[codebeat-url]: https://codebeat.co/projects/github-com-vsouza-awesomeios-com
